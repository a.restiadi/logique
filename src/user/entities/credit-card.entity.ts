import { Column, PrimaryGeneratedColumn } from 'typeorm';

export class CreditCard {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  user_id: number;

  @Column({ type: 'varchar' })
  type: string;

  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  number: string;

  @Column({ type: 'varchar' })
  expired: string;

  @Column({ type: 'varchar' })
  cvv: string;
}
