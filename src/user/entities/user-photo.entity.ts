import { Column, PrimaryGeneratedColumn } from 'typeorm';

export class UserPhoto {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  user_id: number;

  @Column({ type: 'varchar' })
  path: string;
}
