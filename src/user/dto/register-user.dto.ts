import {
  IsEmail,
  IsInt,
  IsNotEmpty,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

const passwordRegEx =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,20}$/;

export class RegisterUserDto {
  @IsString()
  @MinLength(2, { message: 'Name must have atleast 2 characters.' })
  @MaxLength(100, { message: 'Name must have atleast 100 characters.' })
  @IsNotEmpty()
  name: string;

  @IsString()
  @MinLength(5, { message: 'Address must have atleast 5 characters.' })
  @MaxLength(255, { message: 'Address must have atleast 255 characters.' })
  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  @IsEmail(null, { message: 'Please provide valid Email.' })
  email: string;

  @IsNotEmpty()
  @Matches(passwordRegEx, {
    message: `Password must contain Minimum 8 and maximum 20 characters, 
    at least one uppercase letter, 
    one lowercase letter, 
    one number and 
    one special character`,
  })
  password: string;

  @IsInt()
  age: number;

  @IsString({ each: true })
  @IsNotEmpty()
  photos: string[];

  @IsString()
  @IsNotEmpty()
  creditcard_type: string;

  @IsString()
  @IsNotEmpty()
  creditcard_number: string;

  @IsString()
  @IsNotEmpty()
  creditcard_name: string;

  @IsString()
  @IsNotEmpty()
  creditcard_expired: string;

  @IsString()
  @MinLength(3, { message: 'Credit Card CVV must contains 3 numbers.' })
  @MaxLength(3, { message: 'Credit Card CVV must contains 3 numbers.' })
  @IsNotEmpty()
  creditcard_cvv: string;
}

export type RegisterResponse = {
  user_id: number;
};
