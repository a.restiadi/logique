import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { UserPhoto } from './entities/user-photo.entity';
import { CreditCard } from './entities/credit-card.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, UserPhoto, CreditCard])],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
