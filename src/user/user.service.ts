import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RegisterResponse, RegisterUserDto } from './dto/register-user.dto';
// import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { CreditCard } from './entities/credit-card.entity';
import { UserPhoto } from './entities/user-photo.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(CreditCard)
    private ccRepository: Repository<CreditCard>,
    @InjectRepository(UserPhoto)
    private photoRepository: Repository<UserPhoto>,
  ) {}

  async create(data: RegisterUserDto): Promise<RegisterResponse> {
    let user: User = new User();

    user.name = data.name;
    user.email = data.email;
    user.address = data.address;
    user.password = data.password;

    user = await this.userRepository.save(user);

    const photos: number[] = [];

    if (data.photos.length > 0) {
      for (const path of data.photos) {
        const photo: UserPhoto = new UserPhoto();
        photo.user_id = user.id;
        photo.path = path;

        const newPhoto = this.photoRepository.create(photo);
        photos.push(newPhoto.id);
      }

      user.photos = JSON.stringify(photos);
      user = await this.userRepository.save(user);
    }

    const cc: CreditCard = new CreditCard();
    cc.type = data.creditcard_type;
    cc.name = data.creditcard_name;
    cc.number = data.creditcard_number;
    cc.expired = data.creditcard_expired;
    cc.cvv = data.creditcard_cvv;
    cc.user_id = user.id;

    await this.ccRepository.save(cc);

    return { user_id: user.id };
  }

  // findAll(params: ) {
  //   return `This action returns all user`;
  // }

  async findOne(id: number) {
    return await this.userRepository.findOne({ where: { id } });
  }

  // update(id: number, updateUserDto: UpdateUserDto) {
  //   return `This action updates a #${id} user`;
  // }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
