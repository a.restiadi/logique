import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import 'dotenv';

@Injectable()
export class HeaderMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    if (
      process.env.API_KEY &&
      req.headers?.key &&
      req.headers?.key == process.env.API_KEY
    ) {
      next();
    } else {
      return res.json({ error: 'API key is missing.' }).status(500);
    }
  }
}
