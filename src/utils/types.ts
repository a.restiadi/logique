export interface PaginateParams {
  q: string;
  ob: string;
  sb: string;
  of: string;
  lt: string;
}
