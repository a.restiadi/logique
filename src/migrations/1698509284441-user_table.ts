import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class UserTable1698509284441 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'users',
        columns: [
          {
            name: 'id',
            type: 'int4',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'address',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'email',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'password',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'photos',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'creditcard_type',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'creditcard_number',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'creditcard_name',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'creditcard_expired',
            type: 'varchar',
            isNullable: false,
          },
          {
            name: 'creditcard_cvv',
            type: 'varchar',
            isNullable: false,
          },
        ],
      }),
      false,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`DROP TABLE users`);
  }
}
